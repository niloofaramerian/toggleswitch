package com.example.togglebutton

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast

import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

//        val toggleSwitch = findViewById(R.id.multiple_switches) as ToggleSwitch
//        val labels: ArrayList<String> = ArrayList()
//        labels.add("Map")
//        labels.add("Satellite")
//        labels.add("Hybrid")
//        toggleSwitch.setLabels(labels)

        btn1.setOnClickListener {
            Toast.makeText(this , "btn1" , Toast.LENGTH_SHORT).show()
        }

        btn2.setOnClickListener {
            Toast.makeText(this , "btn2" , Toast.LENGTH_SHORT).show()
        }

        btn3.setOnClickListener {
            Toast.makeText(this , "btn3" , Toast.LENGTH_SHORT).show()
        }
    }
}